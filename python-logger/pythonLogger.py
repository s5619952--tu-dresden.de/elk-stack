import random
import logging
import time


def create_logs():
    interval: float = 1.0 # Adjust interval to 0.1 or 0.05 for quicker log generation
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.DEBUG)
    while True:
        random_error: int = random.randint(1, 5)
        if random_error == 1:
            logging.debug("Info zum debuggen.")
        elif random_error == 2:
            logging.info("Ganz normale Info.")
        elif random_error == 3:
            logging.warning("Eine Warnung.")
        elif random_error == 4:
            logging.error("Ein Fehler ist aufgetreten.")
        elif random_error == 5:
            logging.critical("Kritischer Zustand.")
        time.sleep(interval)


if __name__ == '__main__':
    create_logs()
