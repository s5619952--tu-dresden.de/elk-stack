# ELK-Stack

## Vorraussetzungen

- Kubernetes Cluster mit mindestens 3 Nodes
- Helm installiert auf dem Cluster
- Alle Dateien dieses Repositorys im Home-Verzeichnis des Benutzers, welcher den ELK-Stack einrichten und starten möchte

## Aufbau

In diesem Repository befinden sich alle benötigten Config-Files zur Installation eines ELK-Stacks inklusive dreier Python-Logger.
Die Python-Logger generieren in festen Intervallen zufällige Log-Einträge, welche dann von Logstash eingelesen und an elasticsearch übergeben werden. Kibana bietet im Anschluss eine graphische Oberfläche, mit der man die generierten Logs einsehen kann. Logstash speichert die eingelesenen Log-Einträge zusätzlich in den PersistentVolumes der Python-logger, um eine Speicherung der Log-Einträge auch nach einer Deinstallation von Elasticsearch zu gewährleisten.

## Aufsetzen des ELK-Stacks

- StorageClass erstellen: `kubectl apply -f /persistentVolumes/storageClass.yaml`
- alle PersistentVolumes erstellen: `kubectl apply -f /persistentVolumes/X.yaml`
- Elasticsearch zum Helm Repo hinzufügen: `helm repo add elastic https://helm.elastic.co`
- Elasticsearch mittels Helm installieren und dabei die bereitgestellte **values.yaml** benutzen: `helm install elasticsearch elastic/elasticsearch -f ./values.yaml`
- Kibana mittels Helm installieren: `helm install kibana elastic/kibana`
- Logstash installieren: https://www.elastic.co/guide/en/logstash/current/installing-logstash.html
- Docker Image des python-loggers bauen(im **python-logger** Verzeichnis): `docker build -t python_logger_1.0 .` Für schnellere python-logger entsprechend die **pythonLogger.py** anpassen und **interval** entweder auf **0.1** oder **0.05** stellen. Im Anschluss dann ein Docker Image erstellen, welches entweder **python_logger_0.1** für das Interval **0.1** heißt oder **python_logger_0.05** für das Interval **0.05**.
- Verschieben der Datei **logstash/pipelines.yaml** nach **/usr/share/logstash/config/**

## Erstmaliges starten der Python-Logger

- StatefulSets für die erstellten Docker Images erstellen: `kubectl apply -f /python-logger/statefulSet-python-logger-X.yaml`

## Starten des ELK-Stacks

- Weiterleitung des Elasticsearch Ports: `kubectl port-forward svc/elasticsearch-master 9200` (Terminal geöffnet lassen und im Anschluss ein neues verwenden)
- Weiterleitung des Kibana Ports: `kubectl port-forward deployment/kibana-kibana 5601` (Terminal geöffnet lassen und im Anschluss ein neues verwenden)
- Logstash starten: `sudo /usr/share/logstash/bin/logstash`

Nun läuft der ELK-Stack und die generierten Logs werden an Elasticsearch weitergeleitet. Unter localhost:5601 kann man nun in das Web-Interface wechseln.

## Einrichtung Kibana

- In der Seitenleiste auf **Stack Management** klicken und dann unter dem Punkt **Kibana** auf **Index Patterns** drücken
- Neues Index Pattern erstellen, je nach vorher generiertem Python-logger (logstash-1, logstash-2, logstash-3) oder ein generelles Index Pattern für alle python-logger mit **logstash-***
- Nun können die Log-Einträge der Python-logger im Menü unter **Analytics -> Discover** oder **Observability -> Logs** eingesehen werden

## Starten und Stoppen der Python-logger nach dem erstmaligen Starten

- Stoppen der Python-Logger: `kubectl scale --replicas=0 statefulset/python-logger-X`
- Starten der Python-Logger: `kubectl scale --replicas=1 statefulset/python-logger-X`
